package employeeandmanager;

public class Employee {

   double wage;

   double hours;

   String name;

   

    Employee (String name, double wage, double hours) {

        this.name = name;

        this.wage = wage;

        this.hours = hours;

    }

   

    double calculatePay() {

        return wage*hours;

    }

    public String describeEmployee() {

        return "wage = " + wage + ", hours = " + hours + ", name = " + name;

    }

   

}
